SELECT
	ptk.*
FROM
	ptk ptk
JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
JOIN sekolah sekolah on sekolah.sekolah_id = ptkd.sekolah_id
JOIN ref.mst_wilayah kecamatan on kecamatan.kode_wilayah = LEFT(sekolah.kode_wilayah,6)
JOIN ref.mst_wilayah kabupaten on kabupaten.kode_wilayah = kecamatan.mst_kode_wilayah
JOIN ref.mst_wilayah propinsi on propinsi.kode_wilayah = kabupaten.mst_kode_wilayah
WHERE
	ptk.Soft_delete = 0
AND ptkd.Soft_delete = 0
AND ptkd.ptk_induk = 1
AND ptkd.tahun_ajaran_id = 2015
AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
AND (
	ptkd.tgl_ptk_keluar > ta.tanggal_selesai
	OR ptkd.jenis_keluar_id IS NULL
)
AND sekolah.bentuk_pendidikan_id in (13,15)
AND sekolah.status_sekolah in (1,2)
AND ptk.jenis_kelamin in ('L','P')
AND ptk.agama_id in (1,2,3,4,5,6)
