SELECT
	propinsi.nama,
	propinsi.kode_wilayah,
	SUM (CASE WHEN sekolah.status_sekolah in (1) THEN 1 ELSE 0 END ) AS jumlah_negeri,
	SUM (CASE WHEN sekolah.status_sekolah in (2) THEN 1 ELSE 0 END ) AS jumlah_swasta,
	SUM (1) AS jumlah_total
FROM
	sekolah sekolah
JOIN ref.mst_wilayah kecamatan ON kecamatan.kode_wilayah = LEFT (sekolah.kode_wilayah, 6)
JOIN ref.mst_wilayah kabupaten ON kabupaten.kode_wilayah = kecamatan.mst_kode_wilayah
JOIN ref.mst_wilayah propinsi ON propinsi.kode_wilayah = kabupaten.mst_kode_wilayah
WHERE
	soft_delete = 0
AND sekolah.bentuk_pendidikan_id IN (13, 15)
AND sekolah.status_sekolah IN (1, 2)
GROUP BY
	propinsi.kode_wilayah,
	propinsi.nama	