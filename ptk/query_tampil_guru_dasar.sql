SELECT
	ptk.*
FROM
	ptk ptk
JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
WHERE
	ptk.Soft_delete = 0
AND ptkd.Soft_delete = 0
AND ptkd.ptk_induk = 1
AND ptkd.tahun_ajaran_id = 2015
AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
AND (
	ptkd.tgl_ptk_keluar > ta.tanggal_selesai
	OR ptkd.jenis_keluar_id IS NULL
)