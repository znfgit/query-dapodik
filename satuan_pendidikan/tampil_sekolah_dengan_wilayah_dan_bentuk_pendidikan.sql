SELECT
	sekolah.*
FROM
	sekolah sekolah
JOIN ref.mst_wilayah kecamatan on kecamatan.kode_wilayah = LEFT(sekolah.kode_wilayah,6)
JOIN ref.mst_wilayah kabupaten on kabupaten.kode_wilayah = kecamatan.mst_kode_wilayah
JOIN ref.mst_wilayah propinsi on propinsi.kode_wilayah = kabupaten.mst_kode_wilayah
WHERE
	soft_delete = 0
AND sekolah.bentuk_pendidikan_id in (13,15)