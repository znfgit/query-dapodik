SELECT
	count(DISTINCT(pd.peserta_didik_id)) as jumlah
FROM
	peserta_didik pd
JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
JOIN ref.semester sm ON sm.semester_id = rb.semester_id
WHERE
	rpd.Soft_delete = 0
AND pd.Soft_delete = 0
AND (
	rpd.jenis_keluar_id IS NULL
	OR rpd.tanggal_keluar > sm.tanggal_selesai
	OR rpd.jenis_keluar_id = '1'
)
AND sm.tahun_ajaran_id LIKE '2015%'
AND rb.soft_delete = 0
AND ar.soft_delete = 0
AND rb.jenis_rombel = 1